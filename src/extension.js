const vscode = require('vscode');

/**
 * 插件被激活时触发，所有代码总入口
 * @param {*} context 插件上下文
 */
exports.activate = function(context) {
	require('./install')(context) // 安装提示
    /**
     * 跳转到定义部分
     * 会在basepage\basecss\basedatamodel 查找
     */
    require('./jumpDefine')(context) 
};

/**
 * 插件被释放时触发
 */
exports.deactivate = function() {
	require('./unInstall')(context) // 卸载提示
};


