const vscode = require('vscode');
const path = require('path')
const yaml = require('yaml')
const yamlJs = require('yamljs')

/**
 * 右键跳转 跳转 到BasePage/ BaseCss/BaseDatamodel 定义
 * @author xuchen
 */
//  https://code.visualstudio.com/api/references/vscode-api#DefinitionProvider
let provideDefinition = (document,position,token)=>{
    console.log('进入悬浮命令');
    // 获取当前的一些属性， 行数，文件名，文件路径
    const currentLine = document.lineAt(position)
    const currentFileName = document.fileName
    // 获取当前选中词语
    const currentWord = document.getText(document.getWordRangeAtposition(position))
    // 获取当前文件夹，去当前文件夹下面的文件去查找匹配
    const directoryName = path.dirname
    console.log(directoryName);
    // 处理所有的yml文件
    if (/\.yml/.test(currentFileName)) {
        const basePagePath = directoryName+ 'BasePage_en.yml'
        nativeObject = yamlJs.load('file.yml');
        console.log(nativeObject);
        return new vscode.Location(vscode.Uri.file(basePagePath),new vscode.Position(position))
    }
}
// https://code.visualstudio.com/api/references/vscode-api#languages.registerDefinitionProvider
exports = function (context){
    console.log("调用 goto");
    // 实现yml文件的右键跳转，自动去BasePage/ BaseCss/BaseDatamodel去查找文件定义
    context.subscriptions.push(vscode.languages.registerDefinitionProvider(['yaml'],{
        // 调用定义查找函数
        provideDefinition
    }))
}